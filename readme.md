# Stuff
Backlog and change log: https://docs.google.com/document/d/1OqXHIwUFfw_eRrutB16fliRd59PQu9TKjQC7DM1OSBI/edit?usp=sharing
ELO system explained: ELO.pdf

# Offline setup
## Mod files
Clone mod files
```
git clone git@gitlab.com:thisbecasper/duke3dmod.git
```
This new folder will be the root for all Duke Nukem 3D files except for Meltdown.

## Eduke32
Download Eduke32: https://voidpoint.io/StrikerTheHedgefox/eduke32-csrefactor/uploads/bcfe0c901b00b5f50675a99b79101e08/eduke32-oldmp_release_33.zip


Extract all file into the cloned repository (folder name is probably __duke3dmod__)

## Meltdown
Download and install Meltdown: http://duke3donline.com/Setup.zip?r=3633.

Once registered on Meltdown, go into settings and setup the path to eduke32-oldmp.exe you just downloaded and extracted into __duke3dmod__ (Eduke32 EXE option). __REMEMBER TO CLICK APPLY IN THE BOTTOM OF THE SETTINGS SCREEN__.

# Ingame setup
When first playing Duke Nukem 3D through Eduke32 several things have to be set up to achieve the best experience with the mod

## Sound settings
Turn off music

## Game settings
Make sure the screen size slider is all the way to right to disable default UI, custom UI will be rendered by the mod.

You probably also want to make the chat size bigger.

Finally, under the advanced options, turn off "Show player names"

## Video settings
It is recommended to play with color correction, perhaps turning up gamma to 1.10 or 1.20

## Keybindings
Make sure to bind the most important things to comfortable keys. Also utilize the mouse buttons which you can bind under mouse settings
